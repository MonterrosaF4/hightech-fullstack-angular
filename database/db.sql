-- CREAR BASE DE DATOS
CREATE DATABASE database_hightech;
USE database_hightech;

-- CREAR TABLA DE USUARIOS
CREATE TABLE users(
    id INT(11) NOT NULL,
    username VARCHAR(16) NOT NULL,
    password VARCHAR(60) NOT NULL,
    fullname VARCHAR(100) NOT NULL
);

ALTER TABLE users
    ADD PRIMARY KEY (id);

ALTER TABLE users
    ADD UNIQUE (username);

ALTER TABLE users
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT;

DESCRIBE users;

-- CREAR TABLA DE CLIENTES

CREATE TABLE clients(
    id INT(11) NOT NULL,
    cid VARCHAR(20) NOT NULL,
    first_name VARCHAR(16) NOT NULL,
    last_name VARCHAR(16) NOT NULL,
    tc_number VARCHAR(30) NOT NULL,
    cvv VARCHAR(5) NOT NULL,
    expiration_date VARCHAR(30) NOT NULL,
    user_id INT(11),
    created_at timestamp NOT NULL DEFAULT current_timestamp,
    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id)
);

ALTER TABLE clients
    ADD PRIMARY KEY (id);

ALTER TABLE clients
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT;

DESCRIBE clients;