export interface Client {
  id?: number;
  cid?: string;
  first_name?: string;
  last_name?: string;
  tc_type?: string;
  tc_number?: string;
  cvv?: string;
  expiration_date?: string;
  created_at?: Date;
}
