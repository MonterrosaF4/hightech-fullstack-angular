import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Client } from './../models/Client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ClientsService {
  API_URI = 'http://localhost:3000/api';
  constructor(private http: HttpClient) {}
  getClients() {
    return this.http.get(`${this.API_URI}/clients`);
  }

  getClient(id: string) {
    return this.http.get(`${this.API_URI}/clients/${id}`);
  }

  saveClient(client: Client) {
    return this.http.post(`${this.API_URI}/clients`, client);
  }

  deleteClient(id: string) {
    return this.http.delete(`${this.API_URI}/clients/${id}`);
  }

  updateClient(id: string|number, updatedGame: Client): Observable<Client> {
    return this.http.put(`${this.API_URI}/clients/${id}`, updatedGame);
  }
}
