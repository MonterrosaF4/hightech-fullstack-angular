import { Component, OnInit, HostBinding } from '@angular/core';

import { ClientsService } from './../../services/clients.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css'],
})
export class ClientListComponent implements OnInit {
  @HostBinding('class') classes = 'row';

  clients: any = [];
  constructor(private clientsService: ClientsService) {}

  ngOnInit(): void {
    this.getClients();
  }

  getClients() {
    this.clientsService.getClients().subscribe(
      (res) => {
        this.clients = res;
      },
      (err) => console.error(err)
    );
  }

  deleteClient(id: string) {
    this.clientsService.deleteClient(id).subscribe(
      (res) => {
        console.log(res);
        this.getClients();
      },
      (err) => console.error(err)
    );
  }

}
