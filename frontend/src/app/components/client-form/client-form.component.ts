import { ActivatedRoute, Router } from '@angular/router';
import { ClientsService } from './../../services/clients.service';
import { Client } from './../../models/Client';
import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css'],
})
export class ClientFormComponent implements OnInit {
  @HostBinding('class') classes = 'row';

  client: Client = {
    id: 0,
    cid: '',
    first_name: '',
    last_name: '',
    tc_number: '',
    cvv: '',
    expiration_date: '',
    created_at: new Date(),
  };

  edit: boolean = false;
  monthh: string;
  yearr: string;

  constructor(
    private clientsService: ClientsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.clientsService.getClient(params.id).subscribe(
        (res) => {
          this.monthh = res['expiration_date'].substr(5, 2);
          this.yearr = res['expiration_date'].substr(0, 4);
          this.client = res;
          this.edit = true;
        },
        (err) => console.error(err)
      );
    }
  }

  getDate() {
    this.monthh = this.client.expiration_date.substr(5, 2);
    this.yearr = this.client.expiration_date.substr(0, 4);
  }

  guardarNuevoCliente() {
    delete this.client.created_at;
    delete this.client.id;
    this.clientsService.saveClient(this.client).subscribe(
      (res) => {
        console.log(res);
        this.router.navigate(['/clients']);
      },
      (err) => console.error(err)
    );
  }

  updateClient() {
    delete this.client.created_at;
    this.clientsService.updateClient(this.client.id, this.client).subscribe(
      (res) => {
        this.router.navigate(['/clients']);
      },
      (err) => console.error(err)
    );
  }
}
