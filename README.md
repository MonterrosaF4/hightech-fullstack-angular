Esta aplicación permite crear, modificar, visualizar y eliminar clientes con información básica de su tarjeta de crédito.

Se trata de una aplicación hecha en Angular y Typescript que permite hacer un CRUD a un API REST creada en Node.js.

La base de datos usada es MySQL, en esta se almacena toda la información.

La aplicación se trata de una SPA con 3 paginas:

1.  Crear clientes.
2.  Ver clientes.
3.  Prototipo de tarjeta de credito dinamica, en esta pagina se consume un componente de un tercero.

Es necesario dividir el frontend del backend para facilitar los desarrollos y optimizar los servicios del back y las consultas del front, por eso se deben poner en marcha los dos servidores al tiempo.

Para poder iniciar la aplicación es necesario realizar los siguientes pasos:

1.	Tener instalado node en el equipo, se puede descargar desde la siguiente URL: https://nodejs.org/es/
2.	Tener instalado y configurado MySQL
3.  Tener instalado de manera global angular, para ello ejecutar el siguiente comando en una consola: "npm install -g @angular/cli"
4.	Crear una carpeta, abrir una consola de comandos en ese directorio y clonar el repositorio con el siguiente comando: "git clone https://gitlab.com/MonterrosaF4/hightech-fullstack-angular"
5.	Dentro de la carpeta clonada (hightech-fullstack-angular) abrir la carpeta database, en esta carpeta hay un archivo llamado db.sql, copiar todo el código y ejecutarlo con MySQL para preparar la base de datos. (Se maneja la misma base de datos del proyecto de HTML5, si la db ya está creada, omitir este paso)
6.	Dentro de la carpeta clonada (hightech-fullstack-angular) abrir la carpeta backend y posteriormente el archivo ".env".

7.	En este archivo debemos escribir la información para realizar la conexion a nuestra base de datos (host, usuario, contraseña y base de datos), por ejemplo: 

HOST_DB=localhost

USER_DB=root

PASSWORD_DB=toor

DATABASE_DB=database_hightech


NOTA: guardar el archivo ".env"

8.	Si la contraseña de MySQL es de baja complejidad, automáticamente el servidor lanzará un error de seguridad, para corregir este inconveniente es necesario modificar los permisos del usuario con el que se está iniciando sesión con el siguiente comando en SQL: alter user 'user@host' identified with mysql_native_password by 'password' . Se deben modificar los campos de user, host y password que se encuentran dentro de las comillas simples, para mayor información revisar el siguiente link: https://stackoverflow.com/questions/50373427/node-js-cant-authenticate-to-mysql-8-0
9.	Dentro de la ruta del proyecto ingresar a la carpeta backend por medio de la consola e instalar todas las dependencias necesarias con el comando "npm install", al finalizar ejecutar el comando "npm run dev" y dejar corriendo el servidor.
10. Nuevamente dentro de la ruta del proyecto, abrir la carpeta frontend en una nueva terminal por medio de la consola e instalar todas las dependencias necesarias con el comando "npm install", al finalizar ejecutar el comando "ng serve --o" y dejar corriendo el servidor.
11. Cuando finalice la ejecucion del comando ng serve, se abrirá la aplicacion en un navegador.

NOTA: la ejecucion del comando ng serve puede demorarse varios minutos dependiendo de la velocidad del computador