import { Request, Response } from "express";

import pool from "../database";

class ClientsController {
  public async index(req: Request, res: Response) {
    const client = await pool.query("SELECT * FROM clients");
    res.json(client);
  }

  public async getOne(req: Request, res: Response) {
    const { id } = req.params;
    const client = await pool.query('SELECT * FROM clients WHERE id = ?', [id])
    if(client.length > 0){
        return res.json(client[0])
    }
    res.status(404).json({message:'El cliente no existe', status:404})
}

public async create(req: Request, res: Response) {
    console.log(req.body);
    await pool.query("INSERT INTO clients set ?", [req.body]);
    res.json({ message: "Cliente creado" });
}

public async delete(req: Request, res: Response) {
    const { id } = req.params;
    await pool.query('DELETE FROM clients WHERE id = ?', [id])
    res.json({ text: "El cliente fue borrado" });
  }

  public async update(req: Request, res: Response) {
    const { id } = req.params;
    await pool.query('UPDATE clients set ? WHERE id = ?', [req.body, id])
    res.json({ text: "El cliente fue actualizado" });
  }
}

const clientsController = new ClientsController();
export default clientsController;
