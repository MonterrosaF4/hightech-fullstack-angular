"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class ClientsController {
    index(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield database_1.default.query("SELECT * FROM clients");
            res.json(client);
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const client = yield database_1.default.query('SELECT * FROM clients WHERE id = ?', [id]);
            if (client.length > 0) {
                return res.json(client[0]);
            }
            res.status(404).json({ message: 'El cliente no existe', status: 404 });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.body);
            yield database_1.default.query("INSERT INTO clients set ?", [req.body]);
            res.json({ message: "Cliente creado" });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('DELETE FROM clients WHERE id = ?', [id]);
            res.json({ text: "El cliente fue borrado" });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('UPDATE clients set ? WHERE id = ?', [req.body, id]);
            res.json({ text: "El cliente fue actualizado" });
        });
    }
}
const clientsController = new ClientsController();
exports.default = clientsController;
